class Area {
    /**
     * Get the client dimensions
     * @param el
     * @returns {{clientWidth, clientHeight}}
     */
    static getAreaDimensions(el) {
        let {clientHeight, clientWidth} = el;

        return {
            clientHeight,
            clientWidth
        }
    }

    /**
     * Calculate the field size
     * @param w
     * @param h
     */
    static calculateFieldSize(w, h) {
        let size = 12;

        return {
            height: size,
            width: size
        }
    }

    /**
     * Calculate the area size
     * @param clientWidth
     * @param clientHeight
     * @returns {{maxY: number, maxX: number}}
     */
    static calculateAreaSize(clientWidth, clientHeight) {
        const fieldSizes = Area.calculateFieldSize(clientWidth, clientHeight);
        const x = Math.floor(clientWidth / fieldSizes.height);
        const y = Math.floor(clientHeight / fieldSizes.width);

        return {maxX: x, maxY: y};
    }

    /**
     * Build grid
     * @param maxX
     * @param maxY
     */
    static buildGrid(maxX, maxY, el) {
        for (let y = 1; y <= maxY; y++) {
            for (let x = 1; x <= maxX; x++) {
                const gridItem = document.createElement('div');
                gridItem.classList.add('grid-item')

                gridItem.setAttribute('data-x', x);
                gridItem.setAttribute('data-y', y);

                el.appendChild(gridItem);
            }
        }
    }

    /**
     * Get field by coords
     * @param x
     * @param y
     */
    static getFieldByCoords = (x, y) => {
        return document.querySelector(`[data-x='${x}'][data-y='${y}']`);
    }
}