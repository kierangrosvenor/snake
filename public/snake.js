/**
 *
 * Apply snake game logic to any element labeled with id 'snake-game'
 * @param el
 * @constructor
 */




const Snake = (el) => {
    let dimensions = Area.getAreaDimensions(el);
    let areaSize = (Area.calculateAreaSize(dimensions.clientWidth, dimensions.clientHeight));
    let {maxX, maxY} = areaSize;
    let scoreCounter = document.querySelector('[data-score="true"]');
    let fpsCounter = document.querySelector('[data-fps="true"]');
    let scoreHistory = document.querySelector('[data-score-history="true"]');


    Area.buildGrid(maxX, maxY, el)

    el.style.setProperty('--grid-rows', maxY);
    el.style.setProperty('--grid-cols', maxX);

    /**
     * Boolean to denote the game is running
     * @type {boolean}
     */
    let gameRunning = true;

    /**
     * The speed of switch the game runs
     * @type {number}
     */
    let fps = 30;

    /**
     * The direction the snake / user is headed in.
     * @type {string}
     */
    let direction = 'left';

    /**
     * When user interacts the snake can begin moving.
     * @type {boolean}
     */
    let playerInteracted = false;

    /**
     * The snake parts array mapped to coordinates
     * @type {*[]}
     */
    let snakeParts = [];

    /**
     * The current foods mapped to the coordinate
     * @type {*[]}
     */
    let food = [];

    /**
     * The score of the game
     * @type {number}
     */
    let score = 0;

    /**
     * How many parts the snake grows over a series of frames after colliding with food
     * @type {number}
     */
    let foodReward = 3;

    /**
     * History of each games score
     * @type {*[]}
     */
    let scores = []

    scoreCounter.textContent = `${score} pts`


    document.addEventListener('keydown', (e) => {
        if (!gameRunning) {
            return;
        }

        playerInteracted = true;

        switch (e.keyCode) {
            case 37:
                direction = 'left';
                break;
            case 38:
                direction = 'up'
                break;
            case 39:
                direction = 'right'
                break;
            case 40:
                direction = 'down'
                break;
        }
    });

    /**
     * Get coords before moving
     * @returns {[]}
     */
    const getCoordsBeforeMoving = () => {
        const oldPartsCoords = [];

        snakeParts.forEach((part) => {
            oldPartsCoords.push({
                x: part.x,
                y: part.y,
            });
        });

        return oldPartsCoords;
    }

    /**
     * Reset the game back to default state so user can start again
     */
    const resetGame = () => {
        if (score !== 0) {
            scores.push(score);
        }

        gameRunning = false;
        score = 0;
        playerInteracted = false;
        snakeParts = [];
        food = [];

        el.innerHTML = "";

        Area.buildGrid(maxX, maxY, el);

        snakeParts = [
            {
                x: Math.floor(maxX / 2),
                y: Math.floor(maxY / 2)
            },
            {
                x: Math.floor(maxX / 2) - 1,
                y: Math.floor(maxY / 2)
            },
            {
                x: Math.floor(maxX / 2) - 2,
                y: Math.floor(maxY / 2)
            }
        ]

        drawSnake();
        gameRunning = true;
        scoreCounter.textContent = `${score} pts`

        scoreHistory.innerHTML = "";

        scores.forEach((score) => {
            let scoreEl = document.createElement('div');
            scoreEl.classList.add('score');
            scoreEl.textContent = score

            scoreHistory.appendChild(scoreEl);
        });
    }


    let pendingBodyParts = [];

    /**
     * Check that the snake head collided with another part of itself
     * @param to
     */
    const checkSnakePartCollision = (to) => {
        let snakePartCollider = snakeParts.find(x => x.x === to.x && x.y === to.y);
        if (snakePartCollider) {
            //snake hit!
            let snakePartIndex = snakeParts.findIndex(x => x.x === to.x && x.y === to.y);

            gameRunning = false;

            // alert(`Game over! You reached a score of ${score}pts !`);


            resetGame();

            console.log('snakeHit', {
                snakePartIndex,
                snakePart: snakeParts[snakePartIndex]
            })
        }
    }

    /**
     * Check that the snake head collided with a piece of food
     * @param to
     */
    const checkFoodCollision = (to) => {
        //draw snake part
        let crumbCollider = food.find(x => x.x === to.x && x.y === to.y);
        if (crumbCollider) {
            // Found some food!
            // Feed the snake
            let foodIndex = food.findIndex(x => x.x === to.x && x.y === to.y);
            food.splice(foodIndex, 1);
            let foodCell = document.querySelector(`[data-x='${to.x}'][data-y='${to.y}']`);
            foodCell.classList.remove('food');

            let elementsWithSnakeParts = document.querySelectorAll(`.snake-head`);
            let lastPart = elementsWithSnakeParts[elementsWithSnakeParts.length - 1]

            lastPart.classList.remove('snake-tail', 'red')

            // Add body part now, and the rest of a series of frames
            addBodyPart();

            for (let i = 0; i < (foodReward - 1); i++) {
                pendingBodyParts.push(true);
            }

            score += 50;
            scoreCounter.textContent = `${score} pts`
        }
    }

    /**
     * Check for pending body parts and take the first part to be drawn
     * Can be glitchy adding them all over one frame.
     */
    const checkForPendingBodyParts = () => {
        if (pendingBodyParts.length === 0) {
            return;
        }
        addBodyPart();
        pendingBodyParts.splice(0, 1);
    }

    /**
     *
     * @param snakeParts[0]
     * @param {Object<{x: number, y: number}>} to
     */
    const moveSnake = (to) => {
        checkForPendingBodyParts();
        checkFoodCollision(to);
        checkSnakePartCollision(to);

        const oldPartsCoords = getCoordsBeforeMoving();

        for (let i = 0; i < snakeParts.length; i += 1) {
            if (i !== 0) {
                Area.getFieldByCoords(snakeParts[i].x, snakeParts[i].y).classList.remove('snake-tail');
                setPartCoordinate(i, 'x', oldPartsCoords[i - 1].x);
                setPartCoordinate(i, 'y', oldPartsCoords[i - 1].y);
            } else {
                Area.getFieldByCoords(snakeParts[0].x, snakeParts[0].y).classList.remove('snake-head');

                switch (direction) {
                    case 'up':
                        if (snakeParts[0].y === 1) {
                            setPartCoordinate(0, 'y', maxY);
                        } else {
                            setPartCoordinate(0, 'y', snakeParts[0].y - 1);
                        }
                        break;
                    case 'down':
                        if (snakeParts[0].y === maxY) {
                            setPartCoordinate(0, 'y', 1);
                        } else {
                            setPartCoordinate(0, 'y', snakeParts[0].y + 1);
                        }
                        break;
                    case 'left':
                        if (snakeParts[0].x === 1) {
                            setPartCoordinate(0, 'x', maxX);
                        } else {
                            setPartCoordinate(0, 'x', snakeParts[0].x - 1);
                        }
                        break;

                    case 'right':
                        if (snakeParts[0].x === maxX) {
                            setPartCoordinate(0, 'x', 1);
                        } else {
                            setPartCoordinate(0, 'x', snakeParts[0].x + 1);
                        }
                        break;
                }
            }
        }
    }

    /**
     * Set a part coordinate
     * @param index
     * @param axis
     * @param value
     */
    const setPartCoordinate = (index, axis, value) => {
        snakeParts[index][axis] = value;
    }

    /**
     * Add a new body part to the snake.
     */
    const addBodyPart = () => {
        const newBodyPartCoords = {
            x: null,
            y: null,
        };

        let lastPartIndex = snakeParts.length - 1;
        switch (direction) {
            case 'up':
                newBodyPartCoords.x = snakeParts[lastPartIndex].x;
                if (snakeParts[lastPartIndex].y === 1) {
                    newBodyPartCoords.y = maxY;
                } else {
                    newBodyPartCoords.y = snakeParts[lastPartIndex].y - 1;
                }
                break;
            case 'down':
                newBodyPartCoords.x = snakeParts[lastPartIndex].x;
                if (snakeParts[lastPartIndex].y === maxY || snakeParts[lastPartIndex].y > maxY) {
                    newBodyPartCoords.y = 1;
                } else {
                    newBodyPartCoords.y = snakeParts[lastPartIndex].y + 1;
                }
                break;
            case 'left':
                newBodyPartCoords.y = snakeParts[lastPartIndex].y;
                if (snakeParts[lastPartIndex].x === maxX) {
                    newBodyPartCoords.x = 1;
                } else {
                    newBodyPartCoords.x = snakeParts[lastPartIndex].x + 1;
                }
                break;
            case 'right':
                newBodyPartCoords.y = snakeParts[lastPartIndex].y;
                if (snakeParts[lastPartIndex].x === 1) {
                    newBodyPartCoords.x = maxX;
                } else {
                    newBodyPartCoords.x = snakeParts[lastPartIndex].x - 1;
                }
                break;
            default:
                break;
        }

        snakeParts.push({
            x: newBodyPartCoords.x,
            y: newBodyPartCoords.y,
        });
    }

    /**
     * Get food
     * @param min
     * @param max
     * @returns {number}
     */
    const genFood = (min, max) => {
        return Math.floor((Math.random() * (max - min) + min) / 10) * 10;
    }

    /**
     * Make unique string
     * @param length
     * @returns {string}
     */
    const makeUniqueString = (length) => {
        let result = [];
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        return result.join('');
    }

    /**
     * Draw x number of food with randomness
     * @param pieces
     */
    const computeFoods = (pieces) => {

        let seed = makeUniqueString(4);

        for (let i = 0; i < pieces; i++) {
            Math.seedrandom(`${seed}${i}`)


            let generatedFoodCoords = {
                x: genFood(2, maxX - 10),
                y: genFood(2, maxY - 10)
            }

            if (generatedFoodCoords.x === 0 && generatedFoodCoords.y === 0) {
                generatedFoodCoords.x = genFood(2, maxX - 5)
                generatedFoodCoords.y = genFood(2, maxY - 5)

            } else {
                if (generatedFoodCoords.x === 0) {
                    generatedFoodCoords.x += 10;
                }

                if (generatedFoodCoords.y === 0) {
                    generatedFoodCoords.y += 10;
                }
            }

            if (food.findIndex(x => x.x === generatedFoodCoords.x && x.y === generatedFoodCoords.y) === -1) {
                food.push(generatedFoodCoords);
            }


        }
    }


    /**
     * We adjust the snake head when user uses a direction which causes snake to be projected to hit it's neck,
     * swap around the direction of the user input
     * @param snakeHead
     */
    const adjustSnakeHead = (snakeHead) => {
        let snakePartCollider = snakeParts.find(x => x.x === snakeHead.x && x.y === snakeHead.y);
        if (snakePartCollider) {
            let checkedForSnakeNeckCollision = false;

            if (direction === 'up' && checkedForSnakeNeckCollision === false) {
                let snakePartCollisionIndex = snakeParts.findIndex(x => x.x === snakeHead.x && x.y === snakeHead.y - 1);
                if (snakePartCollisionIndex !== -1) {
                    direction = 'down'
                    checkedForSnakeNeckCollision = true;
                }
            }

            if (direction === 'down' && checkedForSnakeNeckCollision === false) {
                let snakePartCollisionIndex = snakeParts.findIndex(x => x.x === snakeHead.x && x.y === snakeHead.y + 1);
                if (snakePartCollisionIndex !== -1) {
                    direction = 'up'
                    checkedForSnakeNeckCollision = true;
                }
            }

            if (direction === 'left' && checkedForSnakeNeckCollision === false) {
                let snakePartCollisionIndex = snakeParts.findIndex(x => x.x === snakeHead.x - 1 && x.y === snakeHead.y);
                if (snakePartCollisionIndex !== -1) {
                    direction = 'right'
                    checkedForSnakeNeckCollision = true;
                }
            }

            if (direction === 'right' && checkedForSnakeNeckCollision === false) {
                let snakePartCollisionIndex = snakeParts.findIndex(x => x.x === snakeHead.x + 1 && x.y === snakeHead.y);
                if (snakePartCollisionIndex !== -1) {
                    direction = 'left'
                }
            }
        }
    }

    /**
     * Lets ask for the game state every 5 seconds
     */
    setInterval(() => {
        console.log('Game state', {
            gameRunning,
            fps,
            playerInteracted,
            food,
            snakeParts,
            score,
            pendingBodyParts
        })
    }, 5000);

    /**
     * Draw the snake
     */
    const drawSnake = () => {
        /* drawing each part of snake */
        for (let i = 0; i < snakeParts.length; i += 1) {
            const className = (i > 0) ? 'snake-tail' : 'snake-head';
            Area.getFieldByCoords(snakeParts[i].x, snakeParts[i].y)
                .classList.add(className);
        }
    }

    resetGame();

// Draw the first part to the snake
// drawSnake();

    let lastCalledTime;
    let counter = 0;
    let fpsArray = [];
    let framesPerSecond = 0;

    /**
     * The core game loop
     */
    function gameLoop() {
        if (!gameRunning) {
            window.requestAnimationFrame(gameLoop);
            return;
        }

        setTimeout(() => {
            if (!lastCalledTime) {
                lastCalledTime = performance.now();
                framesPerSecond = 0;
            }

            let delta = (performance.now() - lastCalledTime) / 1000;
            lastCalledTime = performance.now();
            framesPerSecond = Math.ceil((1 / delta));


            /**
             * Gather performance metric
             * @type {number}
             */

            if (food.length === 0) {
                computeFoods(3);
            } else {
                /**
                 * Draw the foods
                 */
                food.forEach((f, index) => {
                    let cellNode = document.querySelector(`[data-x='${f.x}'][data-y='${f.y}']`)

                    if (cellNode) {
                        cellNode.classList.add('food');
                    } else {
                        food.splice(index, 1);
                    }
                })
            }

            if (playerInteracted === false) {
                window.requestAnimationFrame(gameLoop);
                return;
            }

            let snakeHead = snakeParts[0]
            adjustSnakeHead(snakeHead);

            switch (direction) {
                case 'up':
                    moveSnake({x: snakeHead.x, y: snakeHead.y - 1})
                    break;
                case 'down':
                    moveSnake({x: snakeHead.x, y: snakeHead.y + 1})
                    break;
                case 'left':
                    moveSnake({x: snakeHead.x - 1, y: snakeHead.y})
                    break;
                case 'right':
                    moveSnake({x: snakeHead.x + 1, y: snakeHead.y})
                    break;
            }

            if (counter >= 60) {
                let sum = fpsArray.reduce((a, b) => a + b);
                let average = (sum / fpsArray.length).toFixed(2);
                fpsCounter.textContent = average;
                counter = 0;
            } else {
                if (framesPerSecond !== Infinity) {
                    fpsArray.push(framesPerSecond);
                }
                counter++;
            }

            drawSnake();
            window.requestAnimationFrame(gameLoop);

        }, 1000 / fps)
    }

    window.requestAnimationFrame(gameLoop);
}